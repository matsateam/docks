package net.ukr.wumf;

public class Ship implements Runnable {

	private Port dock;

	private int count;

	private boolean turn;

	public Ship(int count, Port dock, boolean turn) {
		super();
		this.count = count;
		this.turn = turn;
		this.dock = dock;
	}

	public Ship() {
		super();

	}

	@Override
	public void run() {

		dock.razgruz(count, turn);

	}

}
