package net.ukr.wumf;

public class Main {

	public static void main(String[] args) {

		Port dock = new Port();

		Ship shipOne = new Ship(10, dock, false);

		Ship shipTwo = new Ship(10, dock, true);

		Ship shipThree = new Ship(10, dock, true);

		Thread thOne = new Thread(shipOne);

		Thread thTwo = new Thread(shipTwo);

		Thread thThree = new Thread(shipThree);

		thOne.start();

		thTwo.start();

		thThree.start();
	}

}
