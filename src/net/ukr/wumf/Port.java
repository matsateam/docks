package net.ukr.wumf;

public class Port {

	private boolean turn;

	public Port(boolean turn) {
		super();
		this.turn = turn;
	}

	public Port() {
		super();

	}

	public synchronized void razgruz(int count, boolean turn) {

		for (; this.turn == turn;) {

			try {
				wait();
			} catch (InterruptedException e) {

				e.printStackTrace();
			}

		}

		for (int i = 10; i > 0; i--) {

			count = count - 1;
			try {

				wait(500);
			} catch (InterruptedException e) {

				e.printStackTrace();
			}
			System.out.print(count + " ");

		}

		this.turn = !this.turn;

		notify();

	}
}
